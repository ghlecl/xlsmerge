# -*- coding: utf-8 -*-

#
# Copyright 2020 Ghyslain Leclerc
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


import warnings
import datetime
from openpyxl import load_workbook
from openpyxl.utils import column_index_from_string
from openpyxl.workbook import Workbook
from openpyxl.styles import Font, PatternFill

#-----------------------------------------------------------------------------------------
# CONFIGURATION POINTS -> Change these values to change behavior
# first data row == first row which contains data (to skip any so-called 'header')
wb_a_first_data_row = 2
wb_b_first_data_row = 2

# max column == last column in which there is a value
wb_a_max_column = "V"
wb_b_max_column = "AB"

# match column == column whose matching values represent matching case
wb_a_match_column = "A"
wb_b_match_column = "A"

# columns to keep == columns which must be present in the new workbook
wb_a_columns_to_keep = [
   "A", "B", "C", "I", "J", "L", "M", "N"
]
wb_b_columns_to_keep = [
   "K", "L", "V", "X"
]

# new workbook name == name of the new workbook (the one which will be created)
new_wb_name = 'empty_book'

# new workbook sheet name == name of the sheet in the new workbook
new_wb_sheet_name = 'Sheet1'

# headers == values to put as labels in the first row of the new workbook
headers = [
   'ID', 'Lignée', 'G', 'Age', 'DDN', 'S', 'F', 'N', 'FO', 'MO', 'PU', 'R', 'Commentaires'
]




#-----------------------------------------------------------------------------------------
# support function to compensate for the 0-based vs 1-based indexing
def idx_from_str( strg ):
   return column_index_from_string( strg ) - 1


#-----------------------------------------------------------------------------------------
# support function to read a line of data and extract the appropriate columns
def extract_col_values_from_row( row, cols_to_keep ):
   values = []
   for col_name in cols_to_keep:
      val = row[idx_from_str(col_name)].value
      if isinstance(val, datetime.datetime):
         val = val.date()
      values.append( val )
   return values



#-----------------------------------------------------------------------------------------
# Create a dictionnary to hold the data.  Use the id as the dict key, so that you do not
# have to search yourself in the dictionnary for the corresponding entry
data = {}




#-----------------------------------------------------------------------------------------
# Start filling data with workbook 1
# Deactivate the warning about the styles and reactivate them
warnings.simplefilter("ignore")
sheet = load_workbook('Vie 2020-10-19.xlsx', read_only=True).worksheets[0]
warnings.simplefilter("default")

for row in sheet.iter_rows( min_row = wb_a_first_data_row, max_col = idx_from_str( wb_a_max_column ) ):
   data[row[idx_from_str(wb_a_match_column)].value] = extract_col_values_from_row(
      row, wb_a_columns_to_keep
   )




#-----------------------------------------------------------------------------------------
# Continue filling data with workbook 2
# Deactivate the warning about the styles and reactivate them
warnings.simplefilter("ignore")
sheet = load_workbook('Vie 2020-10-19-1.xlsx', read_only=True).worksheets[0]
warnings.simplefilter("default")
for row in sheet.iter_rows( min_row = wb_b_first_data_row, max_col = idx_from_str( wb_b_max_column ) ):
   data[row[idx_from_str(wb_b_match_column)].value].extend( extract_col_values_from_row(
      row, wb_b_columns_to_keep
   ))




#-----------------------------------------------------------------------------------------
# Create new workbook and take active sheet from it
wb = Workbook()
sheet = wb.active
sheet.title = new_wb_sheet_name




#-----------------------------------------------------------------------------------------
# Add header row to the sheet and set font as bold
sheet.append(headers)
for idx in range(len(headers)):
   sheet[1][idx].font = Font(bold=True)




#-----------------------------------------------------------------------------------------
# Add data rows to sheet and set fill color if the sex is male
blue_grey_fill = PatternFill( start_color='6666CC', end_color='6666CC', fill_type='solid' )
total_kept_columns = len(wb_a_columns_to_keep) + len(wb_b_columns_to_keep)
for row_idx, datum in enumerate( data.values() ):
   sheet.append(datum)
   if total_kept_columns > 5 and datum[5] == 'M':
      sheet[row_idx+2][5].fill = blue_grey_fill




#-----------------------------------------------------------------------------------------
# Write workbook
wb.save(filename = new_wb_name + '.xlsx')

